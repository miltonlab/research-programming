/*
 * Copyright (C) 2018 miltonlab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author miltonlab
 */
package research.java;

import research.java.Sorts;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.fail;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.lang.annotation.Repeatable;
import java.util.Scanner;
import java.util.Arrays;
import java.util.Collections;
import java.net.URL;
import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;

/**
 *
 * @author miltonlab
 */

public class SortsTest {    
    static double [] array = new double[10000];
    static double [] orderedArray = new double[10000];
    static Scanner scan = null;
    static PrintWriter pw = null;
    double[] sourceArray;
    double[] expResult;
    long m0, m1 = 0;
    long t0, t1 = 0;
    String method_name = "";
    
    public SortsTest() {
    }
    
    @BeforeClass
    public static void setUpClass(){
        try {
            ClassLoader classLoader = SortsTest.class.getClassLoader();
            URL resource = classLoader.getResource("dataset.txt");
            File file = new File(resource.getPath());  
            int i = 0;
            scan = new Scanner(file);
            pw = new PrintWriter(new FileWriter("metrics.csv", true));
            while (scan.hasNext())
              array[i++] = Double.parseDouble(scan.next());
            double [] copy = java.util.Arrays.copyOf(array, array.length);
            Arrays.sort(copy);
            orderedArray = copy;
            scan.close();  
        } catch(Exception ex){
            System.out.println("Error al incializar los tests: " + ex);
        }
    }
    
    @AfterClass
    public static void tearDownClass() {
        scan.close();
        pw.close();
    }
    
    @Before
    public void setUp() {
        sourceArray = SortsTest.array;
        expResult = SortsTest.orderedArray;
        t0 = System.currentTimeMillis();
        m0 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
    }
    
    public void befforeAssert() {
        t1 = System.currentTimeMillis();
        m1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        pw.printf(Locale.US,"%s, java, %s, %f, %s%n", new Timestamp(t1), 
                this.method_name, (t1-t0)/1000.0, (m1-m0));
    }

    /**
     * Test of bubbleSort method, of class Sorts.
     */
    @Test
    public void testBubbleSort() throws Exception {
        this.method_name = "bubbleSort";
        double[] result = Sorts.bubbleSort(sourceArray);
        this.befforeAssert();
        assertArrayEquals(expResult, result, 0.0001f);
    }

    /**
     * Test of mergeSort method, of class Sorts.
     */
    @Test
    public void testMergeSort() throws Exception {
        this.method_name = "mergeSort";
        double[] result = Sorts.mergeSort(sourceArray);
        this.befforeAssert();
        assertArrayEquals(expResult, result, 0.0001f);
    }

    /**
     * Test of insertionSort method, of class Sorts.
     */
    @Test
    public void testInsertionSort() throws Exception {
        this.method_name = "insertionSort";
        double[] result = Sorts.insertionSort(sourceArray);
        this.befforeAssert();
        assertArrayEquals(expResult, result, 0.0001f);
    }
    
}
