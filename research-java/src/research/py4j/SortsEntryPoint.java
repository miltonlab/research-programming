/*
 * Copyright (C) 2018 miltonlab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author miltonlab
 */

package research.py4j;

import research.java.Sorts;
import py4j.GatewayServer;

/**
 *
 * @author miltonlab
 */

public class SortsEntryPoint {
    
    private Sorts sorts;

    public SortsEntryPoint() {
        sorts = new Sorts();
        this.sorts = sorts;
    }

    public Sorts getSorts() {
        return sorts;
    }
        
    public static void main(String[] args) {    
        GatewayServer gs = new GatewayServer(new SortsEntryPoint());
        gs.start();
        System.out.println("Servidor Gatewat Py4J iniciado ...");
    }
}
