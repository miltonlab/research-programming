#
# Copyright (C) 2018 miltonlab
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
#
# @author miltonlab
#

import unittest
from time import time
from datetime import datetime
import os.path as path
import os
import psutil

import py4j
from py4j.java_gateway import JavaGateway

class TestSorts(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        #
        # IMPORTANT! SortsEntryPoint.java from research-java/research.py4j must be executed first
        #
        gateway = JavaGateway()
        # Py4J entry point
        self.sorts_java = gateway.entry_point.getSorts()
        f = open('dataset.txt', 'r')
        l = [float(n.strip()) for n in f.readlines()]
        double_class = gateway.jvm.double
        # Use py4j arrays
        self.array = gateway.new_array(double_class,len(l))
        for i in range(0,len(l)):
            self.array[i] = l[i]
        l.sort()
        # Use py4j arrays
        self.sortedArray = gateway.new_array(double_class,len(l))
        for i in range(0,len(l)):
            self.sortedArray[i] = l[i]
        f.close()
        self.fw =open('metrics.csv', 'a')
    
    @classmethod
    def tearDownClass(self):
        self.fw.close()
        
    def setUp(self):
        self.t0 = time()
        # get the residente set size memory
        self.m0 = psutil.Process(os.getpid()).memory_info().rss

    def beforeAssert(self):
        self.t1 = time()
        # get the residente set size memory
        self.m1 = psutil.Process(os.getpid()).memory_info().rss
        self.t = self.t1 - self.t0
        self.m = self.m1 - self.m0
        timestamp = datetime.today().isoformat()
        self.fw.write('{0}, py4j, {1}, {2}, {3} \n'.format(timestamp, self.method_name, self.t, self.m))
        
    def test_bubbleSort(self):
        self.method_name = 'bubblesort'
        # throw Java entrypoint
        result = self.sorts_java.bubbleSort(self.array)
        self.beforeAssert()
        # Convert from py4j.java_collections.JavaArray to Pytho list to fast comparision
        self.assertEqual(list(result), list(self.sortedArray))

    def test_insertionSort(self):
        self.method_name = 'insertionsort'
        # throw Java entrypoint
        result = self.sorts_java.insertionSort(self.array)
        self.beforeAssert()
        # Convert from py4j.java_collections.JavaArray to Pytho list to fast comparision
        self.assertEqual(list(result), list(self.sortedArray))

    def test_mergeSort(self):
        self.method_name = 'mergesort'
        # throw Java entrypoint
        result = self.sorts_java.mergeSort(self.array)
        self.assertEqual(list(result), list(self.sortedArray))
            
if __name__ == "__main__":
    unittest.main()