#
# Copyright (C) 2018 miltonlab
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
#
# @author miltonlab
#

import unittest
import os
import psutil
from sorts import *
from time import time
from datetime import datetime

class TestSorts(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        f = open('dataset.txt', 'r')
        self.array = [float(n.strip()) for n in f.readlines()]
        self.sortedArray = self.array.copy()
        self.sortedArray.sort()
        f.close()
        self.fw = open('metrics.csv', 'a')

    @classmethod
    def tearDownClass(self):
        self.fw.close()


    def setUp(self):
        self.t0 = time()
        # get the residente set size memory
        self.m0 = psutil.Process(os.getpid()).memory_info().rss
    
    def beforeAssert(self):
        self.t1 = time()
        # get the residente set size memory
        self.m1 = psutil.Process(os.getpid()).memory_info().rss
        self.t = self.t1 - self.t0
        self.m = self.m1 - self.m0
        timestamp = datetime.today().isoformat()
        self.fw.write('{0}, python, {1}, {2}, {3} \n'.format(timestamp, self.method_name, self.t, self.m))
                
    def test_insertionSort(self):
        self.method_name = 'insertionsort'        
        result = insertionSort(self.array)
        self.beforeAssert()        
        self.assertEqual(result, self.sortedArray)

    def test_mergeSort(self):
        self.method_name = 'mergesort'                
        result = mergeSort(self.array)
        self.beforeAssert()        
        self.assertEqual(result, self.sortedArray)
    
    def test_bubbleSort(self):
        self.method_name = 'bublesort'
        result = bubbleSort(self.array)
        self.beforeAssert()
        self.assertEqual(result, self.sortedArray)
        
if __name__ == "__main__":
    unittest.main()