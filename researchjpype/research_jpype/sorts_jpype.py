#
# Copyright (C) 2018 miltonlab
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
#
# @author miltonlab
#
# Creditos: https://github.com/hustcc/JS-Sorting-Algorithm

from time import time


def bubbleSort(arr):
    for i in range(1, len(arr)):
        for j in range(0, len(arr) - i):
            if arr[j] > arr[j + 1]:
                arr[j], arr[j + 1] = arr[j + 1], arr[j]
    return arr


def insertionSort(arr):
    for i in range(len(arr)):
        preIndex = i - 1
        current = arr[i]
        while preIndex >= 0 and arr[preIndex] > current:
            arr[preIndex + 1] = arr[preIndex]
            preIndex -= 1
        arr[preIndex + 1] = current
    return arr


def mergeSort(arr):
    import math
    if (len(arr) < 2):
        return arr
    middle = len(arr) // 2
    left, right = arr[0:middle], arr[middle:]
    return merge(mergeSort(left), mergeSort(right))


def merge(left, right):
    # Internally left and right are numpy.ndarray
    result = []
    while left and right:
        if left[0] <= right[0]:
            item = left[0]
            left = left[1:]
            result.append(item)
            ###result.append(left.pop(0));
        else:
            item = right[0]
            right = right[1:]            
            result.append(item)
            ###result.append(right.pop(0));
    while left:
        item = left[0]
        left = left[1:]
        result.append(item)
        ###result.append(left.pop(0));
    while right:
        item = right[0]
        right = right[1:]            
        result.append(item)        
        ###result.append(right.pop(0));
    return result

if __name__ == "__main__":
    pass