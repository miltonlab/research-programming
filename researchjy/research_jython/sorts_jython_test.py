import unittest
from sorts_jython import *
from time import time
from datetime import datetime
import os
# Jython arrays
import jarray.array
# Java imports
from java.util import Arrays
from java.lang import Runtime


class TestSorts(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        f= open('dataset.txt', 'r')
        l = [float(n.strip()) for n in f.readlines()]
        self.array = jarray.array(l, 'd')
        aux = Arrays.copyOf(self.array, len(self.array));
        Arrays.sort(aux)
        self.sortedArray = aux
        f.close()
        self.fw = open('metrics.csv', 'a')

    @classmethod
    def tearDownClass(self):
        self.fw.close()

    def setUp(self):
        self.t0 = time()
        # get size memory
        self.m0 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();

    def beforeAssert(self):
        self.t1 = time()
        # get size memory
        self.m1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        self.t = self.t1 - self.t0
        self.m = self.m1 - self.m0
        timestamp = datetime.today().isoformat()
        self.fw.write('{0}, jython, {1}, {2}, {3} \n'.format(timestamp, self.method_name, self.t, self.m))

    def test_bubbleSort(self):
        self.method_name = 'bubblesort'
        result = bubbleSort(self.array)
        self.beforeAssert()
        self.assertEqual(result, self.sortedArray)

    def test_insertionSort(self):
        self.method_name = 'insertionsort'
        result = insertionSort(self.array)
        self.beforeAssert()
        self.assertEqual(result, self.sortedArray)

    def test_mergeSort(self):
        self.method_name = 'mergesort'        
        result = mergeSort(self.array)
        self.beforeAssert()
        # mergeSort() return a list then jarray is need to assert
        self.assertEqual(jarray.array(result,'d'), self.sortedArray)
               
if __name__ == "__main__":
    unittest.main()